#! /bin/bash

gma_file="tardis_extension_tardisman2005.gma"
addon_id="2908897217"

[[ -z $* ]] && echo "Changelog not specified! Aborting." && exit 1
[[ ! -f ./$gma_file ]] && echo "File $gma_file does not exist! Aborting." && exit 2

gmpublish update -id $addon_id -addon $gma_file -changes "$*"
[[ $? = 0 ]] && rm $gma_file
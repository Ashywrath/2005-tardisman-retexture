local Seq = {
    ID = "tardis2005retexture_sequences",

    ["tardis2005_rheostat"] = {
        Controls = {
            "tardis2005_lever1a",
            "tardis2005_sextant",
            "tardis2005_throttle",
        },
        OnFinish = function(self, ply, step, part)
            self.exterior:Demat()
        end,
        OnFail = function(self, ply, step, part)

        end,
        Condition = function(self)
            return not self.exterior:GetData("vortex",false) and not self.exterior:GetData("teleport",false)
        end
    }
}

TARDIS:AddControlSequence(Seq)

local Seq = {
    ID = "tardis2006retexture_sequences",

    ["tardis2005_keyboard"] = {
        Controls = {
            "tardis2005_lever1a",
            "tardis2005_ball1",
            "tardis2005_rotaryswitch",
            "tardis2005_button",
            "tardis2005_throttle",
            "tardis2005_keyboard",
            "tardis2005_sextant",
            "tardis2005_toggles2",
            "tardis2005_greenswitch1",
            "tardis2005_greenswitch2",
            "tardis2005_lever3",
            "tardis2005_lever1b"
        },
        OnFinish = function(self, ply, step, part)
            self.exterior:Demat()
        end,
        OnFail = function(self, ply, step, part)

        end,
        Condition = function(self)
            return not self.exterior:GetData("vortex",false) and not self.exterior:GetData("teleport",false)
        end
    }
}

local Seq = {
    ID = "tardis2007retexture_sequences",

    ["tardis2005_keypad"] = {
        Controls = {
            "tardis2005_toggles2",
            "tardis2005_greenswitch1",
            "tardis2005_greenswitch2",
            "tardis2005_lever2",
            "tardis2005_keypad",
            "tardis2005_keyboard",
            "tardis2005_rotaryswitch",
            "tardis2005_lever3",
            "tardis2005_keyboard",
            "tardis2005_rheostat",
            "tardis2005_lever1b",
            "tardis2005_toggles1",
            "tardis2005_handbrake",
            "tardis2005_throttle",
        },
        OnFinish = function(self, ply, step, part)
            self.exterior:Demat()
        end,
        OnFail = function(self, ply, step, part)

        end,
        Condition = function(self)
            return not self.exterior:GetData("vortex",false) and not self.exterior:GetData("teleport",false)
        end
    }
}

TARDIS:AddControlSequence(Seq)

local Seq = {
    ID = "tardis2008retexture_sequences",

    ["tardis2005_redbutton"] = {
        Controls = {
            "tardis2005_sextant",
            "tardis2005_valve5",
            "tardis2005_handbrake",
            "tardis2005_pump1",
            "tardis2005_throttle",
        },
        OnFinish = function(self, ply, step, part)
            self.exterior:Demat()
        end,
        OnFail = function(self, ply, step, part)
            -- fail stuff
        end,
        Condition = function(self)
            return not self.exterior:GetData("vortex",false) and not self.exterior:GetData("teleport",false)
        end
    }
}

TARDIS:AddControlSequence(Seq)

local Seq = {
    ID = "tardis2009retexture_sequences",

    ["tardis2005_keypad"] = {
        Controls = {
            "tardis2005_throttle",
        },
        OnFinish = function(self, ply, step, part)
            self.exterior:Demat()
        end,
        OnFail = function(self, ply, step, part)
            -- fail stuff
        end,
        Condition = function(self)
            return not self.exterior:GetData("vortex",false) and not self.exterior:GetData("teleport",false)
        end
    }
}

TARDIS:AddControlSequence(Seq)


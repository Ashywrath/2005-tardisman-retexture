TARDIS:AddInteriorTemplate("tardisman_coral_sounds_2005", {
	Interior = {
		IdleSound={
			{
				path="toast/coral/coral_idle_interior.wav",
				volume=1
			}
		},
		Sounds={
			Teleport={
				demat="toast/coral/demat_int9th.wav", -- Demat Sound
				mat="toast/coral/mat_int9th.wav", -- Mat Sound
				mat_fail = "toast/coral/smeef.wav",
				demat_fail = "toast/coral/smeef.wav",
				--mat_fail = "toast/coral/mat_fail.wav",
				fullflight="toast/coral/fast_demat2005int.wav",
				interrupt ="toast/coral/inter_int9th.wav"
			},
			Power={
				On="toast/coral/poweron.wav", -- Power On
				Off="toast/coral/poweroff.wav" -- Power Off
			},
			FlightLoop="toast/coral/int_flightloop9th.wav",
			--Power = {
			--	On = "Poogie/toyota/power/powerup_2012.wav",
			--	Off = "Poogie/toyota/power/poweroff_2013.wav"
			--},
			--FlightLoop="Poogie/toyota/flight/flight_loop_2012.wav",
		},
		IdleSound={
			{
				path="toast/coral/coral_idle_interior.wav",
				volume=1
			}
		},
		tardis2005_rotor_speed = 0.5, -- should probably be in sync with the sounds
	},
	Exterior = {
		Sounds={
			Teleport={
				demat="toast/coral/demat_ext9th.wav",
				mat="toast/coral/mat_ext.wav",
				interrupt ="toast/coral/inter9th.wav",
				demat_fail = "toast/coral/smeef.wav"
			},
		},
		Teleport = {
			SequenceSpeed = 0.50,
			SequenceSpeedFast = 0.99,
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_coral_sounds_2006_2008", {
	Interior = {
		Sounds={
			Teleport={
				demat="toast/coral/demat_int.wav",
				mat="toast/coral/mat_int.wav",
				--mat_fail = "toast/coral/smeef.wav",
				demat_fail = "toast/coral/smeef.wav",
				mat_fail = "toast/coral/mat_fail.wav", -- Demat Fail Sound
				fullflight="toast/coral/fast_dematint.wav",
				interrupt ="toast/coral/inter_int2006.wav"
				--fullflight="toast/coral/fast_demat.wav"
			},
			FlightLoop="toast/coral/flight_loop.wav",
			Power={
				On="toast/coral/poweron.wav", -- Power On
				Off="toast/coral/poweroff.wav" -- Power Off
			},
			--Teleport={
			--	demat="Poogie/toyota/demat/demat_2012_snowmen.wav",
			--	fullflight = "Poogie/toyota/full/full_2012.wav",
			--	mat="Poogie/toyota/mat/mat_2013_NOTD.wav",
			--	demat_fail="Poogie/toyota/others/demat_fail_2.wav",
			--},
			--FlightLoop="Poogie/toyota/flight/flight_loop_2012.wav",
			--Power = {
			--	On = "Poogie/toyota/power/powerup_2013.wav",
			--	Off = "Poogie/toyota/power/poweroff_2013.wav"
			--},
		},
		tardis2005_rotor_speed = 0.5, -- should probably be in sync with the sounds
	},
	Exterior = {
		Sounds={
			Teleport={
				demat="toast/coral/demat.wav",
				mat="toast/coral/mat.wav",
				--mat_fail = "toast/coral/mat_fail.wav",
				fullflight="toast/coral/fast_demat.wav",
				interrupt ="toast/coral/inter2006.wav"
			},
		},
		Teleport = {
			SequenceSpeed = 0.63,
			SequenceSpeedFast = 0.99,
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_coral_sounds_2009", {
	Interior = {
		Sounds={
			Teleport={
				demat="toast/coral/2009demat_int.wav",
				mat="toast/coral/2009mat_int.wav",
				mat_fail = "toast/coral/smeef.wav",
				demat_fail = "toast/coral/smeef.wav",
				mat_fail = "toast/coral/mat_fail.wav",
				fullflight="toast/coral/fast_demat2009int.wav",
				interrupt ="toast/coral/2009_INTERINT.wav"
			},
			FlightLoop="toast/coral/flight_loop.wav",
			Power={
				On="toast/coral/poweron.wav", -- Power On
				Off="toast/coral/poweroff.wav" -- Power Off
			},
			--Teleport={
			--	demat="Poogie/toyota/demat/demat_NOTD.wav",
			--	fullflight = "Poogie/toyota/full/full_2012.wav",
			--	mat="Poogie/toyota/mat/mat_2013_NOTD.wav",
			--	demat_fail="Poogie/toyota/others/demat_fail_2.wav",
			--},
			--Power = {
			--	On = "Poogie/toyota/power/powerup_2013.wav",
			--	Off = "Poogie/toyota/power/poweroff_2013.wav"
			--},
			--FlightLoop="Poogie/toyota/flight/flight_loop_2012.wav",
		},
		tardis2005_rotor_speed = 0.5, -- should probably be in sync with the sounds
	},
	Exterior = {
		Sounds={
			Teleport={
				demat="toast/coral/2009demat.wav",
				mat="toast/coral/2009mat.wav",
				fullflight="toast/coral/fast_demat2009.wav",
				interrupt ="toast/coral/inter2006.wav"
			},
		},
		Teleport = {
			SequenceSpeed = 0.83,
			SequenceSpeedFast = 0.99,
		},
	},
})
TARDIS:AddInteriorTemplate("tardisman_coral_sounds_house", {
	Interior = {
		Sounds={
			Teleport={
				demat="toast/coral/coral_house_demat.wav", -- Demat Sound
				mat="toast/coral/coral_house_mat.wav", -- Mat Sound
				demat_fail = "toast/coral/smeef.wav",
				mat_fail = "toast/coral/mat_fail.wav",
				fullflight="toast/coral/fast_demat2005int.wav",
				interrupt ="toast/coral/inter_int9th.wav"
			},
			FlightLoop="toast/coral/flight_loop.wav",
			Power={
				On="toast/coral/coral_powerup_house.wav", -- Power On
				Off="toast/coral/coral_powerdown_house.wav" -- Power Off
			},
		},
		IdleSound={
			{
				path="toast/coral/coral_idle_interior_house.wav",
				volume=1
			}
		},
		tardis2005_rotor_speed = 0.2, -- should probably be in sync with the sounds
	},
	Exterior = {
		Sounds={
			Teleport={
				demat="toast/coral/2009demat.wav",
				mat="toast/coral/2009mat.wav",
				fullflight="toast/coral/fast_demat2009.wav",
				interrupt ="toast/coral/inter2006.wav"
			},
		},
		Teleport = {
			SequenceSpeed = 0.50,
			SequenceSpeedFast = 0.99,
		},
	},
})
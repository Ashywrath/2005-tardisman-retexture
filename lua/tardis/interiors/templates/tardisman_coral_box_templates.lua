TARDIS:AddInteriorTemplate("tardisman_box_common", {
	Interior = {
		Sounds={
			Door={
				enabled=true,
				open="toast/coral/door_open.wav",
				close="toast/coral/door_close.wav",
			},
		},
	},
	Exterior = {
		Light = {
			color = Color(255, 236, 153),
			dynamicsize = 180
		},
		Sounds={
			Door={
				enabled=true,
				open="toast/coral/door_open.wav",
				close="toast/coral/door_close.wav",
			},
		},
		ProjectedLight = {
			brightness = 1,
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_ab_box", {
	Exterior = {
		Fallback={
			pos=Vector(50, 0, 10),
			ang=Angle(0, 0, 0)
		},
		Light={
			pos=Vector(0, 0, 121),
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_c_box", {
	Interior = {
		Parts = {
			door = {
				model = "models/doctorwho1200/exterior_c/doors.mdl",
				posoffset = Vector(5.5,0,-48.971),
				angoffset = Angle(0, 180, 0),
			}
		},
	},
	Exterior = {
		Model = "models/doctorwho1200/exterior_c/exterior_c.mdl",
		Portal = {
			thickness = 32.56,
		},
		Fallback = {
			pos = Vector(50, 0, 7),
			ang = Angle(0, 0, 0)
		},
		Parts = {
			door = {
				model = "models/doctorwho1200/exterior_c/doors_ext.mdl",
				posoffset = Vector(-5.5,0,-48.971),
				angoffset = Angle(0, 0, 0)
			}
		},
		TextureSets = {
			all_textures = {
				prefix = "models/tardisman/exterior_c/",
			},
		},
	},
})

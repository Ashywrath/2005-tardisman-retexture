TARDIS:AddInteriorTemplate("tardisman_colors_2005", {
	Interior = {
		LightOverride = {
			basebrightness = 0.22,
		},
		Light = {
			color = Color(16, 255, 215),
			warncolor = Color(16, 255, 215),
			brightness = 1
		},
		Lights = {
			{
				color = Color(255, 231, 94, 204),
				brightness = 0.8,
				warncolor = Color(182, 151, 83),
			},
			{
				color = Color(255, 187, 60, 206),
				brightness = 0.7,
				warncolor = Color(136, 113, 70),
			},
		},
		Lamps = {
			wall_1 = {
				color = Color(125, 68, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.86,
				pos = Vector(115.4423828125, -193.1455078125, 18.2333984375),
				ang = Angle(-44.985950469971, -55.548427581787, 44.996780395508),

				warn = {
					color = Color(255, 60, 60)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_2 = {
				color = Color(125, 68, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.86,
				pos = Vector(-223.458984375, -1.6337890625, 26.125),
				ang = Angle(-45.023929595947, 179.95172119141, 45.01872253418),

				warn = {
					color = Color(255, 60, 60)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			console_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 123.63636016846,
				distance = 200,
				brightness = 0.4,
				pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
				ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
				shadows = true,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			door_off = {
				color = Color(0,0,0),
				texture = "effects/flashlight/soft",
				fov = 90,
				distance = 200,
				brightness = 0.3,
				pos = Vector(230, 0, 140),
				ang = Angle(70, 0, 0),
				shadows = false,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			wall_3 = {
				color = Color(125, 68, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.86,
				pos = Vector(114.9189453125, 192.8037109375, 26.8447265625),
				ang = Angle(-44.995010375977, 56.045471191406, 45.01008605957),

				warn = {
					color = Color(255, 60, 60)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_4 = {
				color = Color(175, 68, 6),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 503.63635253906,
				brightness = 0.4,
				shadows = true,
				pos = Vector(98.563598632813, 172.79925537109, 218.51171875),
				ang = Angle(66.684761047363, -122.35398864746, -131.32559204102),

				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
		},
		TextureSets = {
			all_textures = {
				prefix = "models/tardisman/coral_9th/",
			},
			power_textures = {
				prefix = "models/tardisman/coral_9th/",
			},
		},
	},
})
TARDIS:AddInteriorTemplate("tardisman_colors_DOTD", {
	Interior = {
		LightOverride = {
			basebrightness = 0.12,
		},
		Light = {
			color = Color(0, 233, 233),
			warncolor = Color(16, 255, 215),
			brightness = 0.78
		},
		Lights = {
			{
				color = Color(0, 0, 0, 204),
				brightness = 0.8,
				warncolor = Color(0, 0, 0),
			},
			{
				color = Color(0, 0, 0, 206),
				brightness = 0.7,
				warncolor = Color(0, 0, 0),
			},
		},
		Lamps = {
			console_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 123.63636016846,
				distance = 200,
				brightness = 0.4,
				pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
				ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
				shadows = true,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_1 = {
				color = Color(104, 52, 3),
				texture = "effects/flashlight/soft",
				fov = 80,
				distance = 616.36364746094,
				brightness = 0.5,
				pos = Vector(-95.349609375, 84.55859375, 86.9521484375),
				ang = Angle(8.034291267395, -39.956832885742, 152.51672363281),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_2 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.77,
				pos = Vector(276.23291015625, -95.972396850586, 11.181640625),
				ang = Angle(-74.478675842285, -26.848712921143, 154.96101379395),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_3 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(110.03759765625, -257.27835083008, 20.2685546875),
				ang = Angle(-79.406143188477, -66.843933105469, 146.86637878418),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_4 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-58.076171875, -254.6669921875, 20.078125),
				ang = Angle(-79.406143188477, -98.392051696777, 146.86636352539),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_5 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-170.37475585938, -198.802734375, 12.78125),
				ang = Angle(-79.406143188477, -131.26023864746, 146.86636352539),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_6 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-206.408203125, 209.6337890625, 19.92578125),
				ang = Angle(-81.35474395752, 138.51747131348, 146.42248535156),
				shadows = true,

				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_7 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-104.0517578125, 256.6181640625, 13.9130859375),
				ang = Angle(-83.132545471191, 107.3970413208, 144.52496337891),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_8 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(78.666015625, 283.728515625, 15.9140625),
				ang = Angle(-79.406135559082, 64.003898620605, 146.86636352539),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_9 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(189.7607421875, 217.34164428711, 11.6376953125),
				ang = Angle(-79.406135559082, 30.60778427124, 146.86633300781),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_10 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-309.4169921875, -75.865936279297, 12.958984375),
				ang = Angle(-83.747146606445, 12.364686965942, -167.90577697754),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_11 = {
				color = Color(85, 80, 51),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-314.90112304688, 45.0693359375, 11.533203125),
				ang = Angle(-83.74715423584, -20.237749099731, -167.90577697754),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_0_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.7699999809265,
				pos = Vector(231.56860351563, -193.26171875, 18.859375),
				ang = Angle(-74.503662109375, -40.788387298584, 155.1356048584),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_1_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.7699999809265,
				pos = Vector(289.44555664063, 78.523681640625, 10.453125),
				ang = Angle(-74.47868347168, 34.795299530029, 154.96101379395),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_2_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.77,
				pos = Vector(276.23291015625, -95.972396850586, 11.181640625),
				ang = Angle(-74.478675842285, -26.848712921143, 154.96101379395),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_3_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(110.03759765625, -257.27835083008, 20.2685546875),
				ang = Angle(-79.406143188477, -66.843933105469, 146.86637878418),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_4_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-58.076171875, -254.6669921875, 20.078125),
				ang = Angle(-79.406143188477, -98.392051696777, 146.86636352539),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_5_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-170.37475585938, -198.802734375, 12.78125),
				ang = Angle(-79.406143188477, -131.26023864746, 146.86636352539),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_6_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-206.408203125, 209.6337890625, 19.92578125),
				ang = Angle(-81.35474395752, 138.51747131348, 146.42248535156),
				shadows = true,
				nopower= true,
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_7_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-104.0517578125, 256.6181640625, 13.9130859375),
				ang = Angle(-83.132545471191, 107.3970413208, 144.52496337891),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_8_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(78.666015625, 283.728515625, 15.9140625),
				ang = Angle(-79.406135559082, 64.003898620605, 146.86636352539),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_9_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(189.7607421875, 217.34164428711, 11.6376953125),
				ang = Angle(-79.406135559082, 30.60778427124, 146.86633300781),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_10_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-309.4169921875, -75.865936279297, 12.958984375),
				ang = Angle(-83.747146606445, 12.364686965942, -167.90577697754),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_11_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-314.90112304688, 45.0693359375, 11.533203125),
				ang = Angle(-83.74715423584, -20.237749099731, -167.90577697754),
				shadows = true,
				nopower= true,
				
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			door_off = {
				color = Color(0,0,0),
				texture = "effects/flashlight/soft",
				fov = 90,
				distance = 200,
				brightness = 0.3,
				pos = Vector(230, 0, 140),
				ang = Angle(70, 0, 0),
				shadows = false,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
		},
		TextureSets = {
			all_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
			power_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
		},
	},
})
TARDIS:AddInteriorTemplate("tardisman_colors_2006", {
	Interior = {
		LightOverride = {
			basebrightness = 0.07,
		},
		Light = {
			color = Color(16, 255, 215),
			warncolor = Color(16, 255, 215),
			brightness = 1.5
		},
		Lights = {
			{
				color = Color(255, 231, 94, 204),
				brightness = 0.8,
				warncolor = Color(182, 151, 83),
			},
			{
				color = Color(255, 187, 60, 206),
				brightness = 0.7,
				warncolor = Color(136, 113, 70),
			},
		},
		Lamps = {
			wall_1 = {
				color = Color(125, 97, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.6,
				pos = Vector(115.4423828125, -193.1455078125, 18.2333984375),
				ang = Angle(-44.985950469971, -55.548427581787, 44.996780395508),

				warn = {
					color = Color(155,50,50)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			console_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 123.63636016846,
				distance = 200,
				brightness = 0.4,
				pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
				ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
				shadows = true,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			door_off = {
				color = Color(0,0,0),
				texture = "effects/flashlight/soft",
				fov = 90,
				distance = 200,
				brightness = 0.3,
				pos = Vector(230, 0, 140),
				ang = Angle(70, 0, 0),
				shadows = false,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			wall_2 = {
				color = Color(125, 97, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.6,
				pos = Vector(-223.458984375, -1.6337890625, 26.125),
				ang = Angle(-45.023929595947, 179.95172119141, 45.01872253418),

				warn = {
					color = Color(155,50,50)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_3 = {
				color = Color(125, 97, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.6,
				pos = Vector(114.9189453125, 192.8037109375, 26.8447265625),
				ang = Angle(-44.995010375977, 56.045471191406, 45.01008605957),

				warn = {
					color = Color(155,50,50)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_4 = {
				color = Color(42, 245, 200),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 400,
				brightness = 1,
				pos = Vector(60.883544921875, -0.61474609375, 185.921875),
				ang = Angle(44.999996185303, 0.022677728906274, 44.999984741211),
			},
			under_the_console = {
				color = Color(42, 245, 200),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 400,
				brightness = 1,
				pos = Vector(0, 0, 0),
				ang = Angle(-90, 0, 0),
			},
		},
		TextureSets = {
			all_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
			power_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_colors_2007", {
	Interior = {
		LightOverride = {
			basebrightness = 0.17,
		},
		Light = {
			color = Color(16, 255, 215),
			warncolor = Color(16, 255, 215),
			brightness = 0.25
		},
		Lights = {
			{
				color = Color(255, 231, 94, 204),
				brightness = 0.8,
				warncolor = Color(182, 151, 83),
			},
			{
				color = Color(255, 187, 60, 206),
				brightness = 0.7,
				warncolor = Color(136, 113, 70),
			},
		},
		Lamps = {
			wall_1 = {
				color = Color(226, 194, 164),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.1,
				pos = Vector(35.08837890625, -66.75048828125, 215.359375),
				ang = Angle(68.821922302246, -57.793685913086, -122.65737915039),

				warn = {
					color = Color(155,50,50)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_2 = {
				color = Color(226, 194, 164),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.1,
				pos = Vector(-84.211181640625, -2.478515625, 218.2197265625),
				ang = Angle(67.282157897949, -174.31925964355, -86.926818847656),

				warn = {
					color = Color(155,50,50)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_3 = {
				color = Color(226, 194, 164),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 1.1,
				pos = Vector(21.555786132813, 74.853515625, 217.6494140625),
				ang = Angle(72.478538513184, 72.291961669922, -137.94195556641),

				warn = {
					color = Color(155,50,50)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			console_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 123.63636016846,
				distance = 200,
				brightness = 0.4,
				pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
				ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
				shadows = true,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			door_off = {
				color = Color(0,0,0),
				texture = "effects/flashlight/soft",
				fov = 90,
				distance = 200,
				brightness = 0.3,
				pos = Vector(230, 0, 140),
				ang = Angle(70, 0, 0),
				shadows = false,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			wall_4 = {
				color = Color(226, 194, 164),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 300,
				brightness = 0.25,
				pos = Vector(233.13061523438, -0.66015625, 219.779296875),
				ang = Angle(52.830783843994, -177.60000610352, -163.38583374023),

				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			under_the_console = {
				color = Color(42, 245, 200),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 200,
				brightness = 0.5,
				pos = Vector(0, 0, 0),
				ang = Angle(-90, 0, 0),
			},
		},
		TextureSets = {
			all_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
			power_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_colors_2008", {
	Interior = {
		LightOverride = {
			basebrightness = 0.35,
		},
		Light = {
			color = Color(16, 255, 215),
			warncolor = Color(16, 255, 215),
			brightness = 2,
			falloff = 5
		},
		Lights = {
			{
				color = Color(255, 231, 94, 204),
				warncolor = Color(182, 151, 83),
			},
			{
				color = Color(255, 187, 60, 206),
				warncolor = Color(136, 113, 70),
			},
		},
		Lamps = {
			wall_1 = {
				color = Color(125, 97, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 2.2,
				pos = Vector(95.2138671875, -161.62109375, 246.5625),
				ang = Angle(62.791618347168, -59.290927886963, 53.157371520996),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			console_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 123.63636016846,
				distance = 200,
				brightness = 0.4,
				pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
				ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
				shadows = true,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			door_off = {
				color = Color(0,0,0),
				texture = "effects/flashlight/soft",
				fov = 90,
				distance = 200,
				brightness = 0.3,
				pos = Vector(230, 0, 140),
				ang = Angle(70, 0, 0),
				shadows = false,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			wall_2 = {
				color = Color(125, 97, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 2.2,
				pos = Vector(-181.0166015625, -0.618408203125, 243.939453125),
				ang = Angle(62.791652679443, -173.36100769043, 53.157348632813),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_3 = {
				color = Color(125, 97, 21),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 2.2,
				pos = Vector(89.763671875, 153.7041015625, 244.904296875),
				ang = Angle(62.791481018066, 69.615928649902, 53.157470703125),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
		},
		TextureSets = {
			all_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
			power_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_colors_2009", {
	Interior = {
		LightOverride = {
			basebrightness = 0.35,
		},
		Light = {
			color = Color(16, 255, 215),
			warncolor = Color(16, 255, 215),
			brightness = 1,
			falloff = 5
		},
		Lights = {
			{
				color = Color(255, 187, 0, 204),
				warncolor = Color(182, 151, 83),
			},
			{
				color = Color(255, 187, 60, 206),
				warncolor = Color(136, 113, 70),
			},
		},
		Lamps = {
			wall_1 = {
				color = Color(255, 230, 0),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 2,
				pos = Vector(92.44140625, -148.4619140625, 139.7763671875),
				ang = Angle(3.8952269554138, -56.272190093994, 45.451187133789),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_2 = {
				color = Color(194, 181, 66),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 2.,
				pos = Vector(-181.0166015625, -0.618408203125, 243.939453125),
				ang = Angle(62.791652679443, -173.36100769043, 53.157348632813),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_3 = {
				color = Color(255, 230, 0),
				texture = "effects/flashlight/soft",
				fov = 110.90908813477,
				distance = 864.36364746094,
				brightness = 2,
				pos = Vector(293.55395507813, -95.082397460938, 3.5224609375),
				ang = Angle(-79.629493713379, -38.522308349609, -36.018341064453),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_4 = {
				color = Color(255, 230, 0),
				texture = "effects/flashlight/soft",
				fov = 107.27272796631,
				distance = 864.36364746094,
				brightness = 2,
				pos = Vector(205.5263671875, -35.07177734375, 232.986328125),
				ang = Angle(38.894744873047, -15.654410362244, -37.454315185547),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_5 = {
				color = Color(255, 230, 0),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 2,
				pos = Vector(-99.500335693359, 173.640625, 245.2490234375),
				ang = Angle(36.284954071045, 117.23764801025, 53.537448883057),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			console_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 123.63636016846,
				distance = 200,
				brightness = 0.4,
				pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
				ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
				shadows = true,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			door_off = {
				color = Color(0,0,0),
				texture = "effects/flashlight/soft",
				fov = 90,
				distance = 200,
				brightness = 0.3,
				pos = Vector(230, 0, 140),
				ang = Angle(70, 0, 0),
				shadows = false,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			wall_6 = {
				color = Color(255, 230, 0),
				texture = "effects/flashlight/soft",
				fov = 136.36363220215,
				distance = 524,
				brightness = 2,
				pos = Vector(87.1171875, 183.95141601563, 243.4609375),
				ang = Angle(30.95679473877, 66.744453430176, 56.428176879883),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			wall_7 = {
				color = Color(255, 153, 0),
				texture = "effects/flashlight/soft",
				fov = 170,
				distance = 524,
				brightness = 2,
				pos = Vector(-90.5791015625, -154.8984375, 241.486328125),
				ang = Angle(50.736087799072, -116.38425445557, 55.085540771484),

				warn = {
					color = Color(179,15,15)
				},
				off_warn = {
					color = Color(180,0,0)
				},
			},
			lamp_0_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.7699999809265,
				pos = Vector(231.56860351563, -193.26171875, 18.859375),
				ang = Angle(-74.503662109375, -40.788387298584, 155.1356048584),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_1_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.7699999809265,
				pos = Vector(289.44555664063, 78.523681640625, 10.453125),
				ang = Angle(-74.47868347168, 34.795299530029, 154.96101379395),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_2_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.77,
				pos = Vector(276.23291015625, -95.972396850586, 11.181640625),
				ang = Angle(-74.478675842285, -26.848712921143, 154.96101379395),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_3_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(110.03759765625, -257.27835083008, 20.2685546875),
				ang = Angle(-79.406143188477, -66.843933105469, 146.86637878418),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_4_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-58.076171875, -254.6669921875, 20.078125),
				ang = Angle(-79.406143188477, -98.392051696777, 146.86636352539),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_5_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-170.37475585938, -198.802734375, 12.78125),
				ang = Angle(-79.406143188477, -131.26023864746, 146.86636352539),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_6_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-206.408203125, 209.6337890625, 19.92578125),
				ang = Angle(-81.35474395752, 138.51747131348, 146.42248535156),
				shadows = true,
				nopower= true,
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_7_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-104.0517578125, 256.6181640625, 13.9130859375),
				ang = Angle(-83.132545471191, 107.3970413208, 144.52496337891),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_8_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(78.666015625, 283.728515625, 15.9140625),
				ang = Angle(-79.406135559082, 64.003898620605, 146.86636352539),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_9_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(189.7607421875, 217.34164428711, 11.6376953125),
				ang = Angle(-79.406135559082, 30.60778427124, 146.86633300781),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_10_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-309.4169921875, -75.865936279297, 12.958984375),
				ang = Angle(-83.747146606445, 12.364686965942, -167.90577697754),
				shadows = true,
				nopower= true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
			},
			lamp_11_off = {
				color = Color(0, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-314.90112304688, 45.0693359375, 11.533203125),
				ang = Angle(-83.74715423584, -20.237749099731, -167.90577697754),
				shadows = true,
				nopower= true,
				
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(179, 161, 3),
				},
				off = {
					color = Color(179, 161, 3),
				},
				},
		},
		TextureSets = {
			all_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
			power_textures = {
				prefix = "models/tardisman/coral_10th/",
			},
		},
	},
})

TARDIS:AddInteriorTemplate("tardisman_colors_house", {
	Interior = {
		LightOverride = {
			basebrightness = 0.00,
		},
		Light = {
			color = Color(0, 0, 0),
			warncolor = Color(0, 0, 0),
			brightness = 1,
			falloff = 5
		},
		Lights = {
			{
				color = Color(255, 255, 255, 206),
				brightness = 0.4,
				warncolor = Color(0, 0, 0),
			},
			{
				color = Color(255, 255, 255, 206),
				brightness = 0.4,
				warncolor = Color(0, 0, 0),
			},
		},
		Lamps = {
			console_off = {
				color = Color(253, 198, 80),
				texture = "effects/flashlight/soft",
				fov = 123.63636016846,
				distance = 200,
				brightness = 0.4,
				pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
				ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
				shadows = true,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(0, 0, 0)
				},
				off = {
					color = Color(0, 0, 0)
				},
			},
			door_off = {
				color = Color(255, 245, 165),
				texture = "effects/flashlight/soft",
				fov = 90,
				distance = 200,
				brightness = 0.3,
				pos = Vector(230, 0, 140),
				ang = Angle(70, 0, 0),
				shadows = false,
	
				nopower = true,
	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(0, 0, 0)
				},
				off = {
					color = Color(0, 0, 0)
				},
			},
			lamp_0 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.7699999809265,
				pos = Vector(231.56860351563, -193.26171875, 18.859375),
				ang = Angle(-74.503662109375, -40.788387298584, 155.1356048584),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_1 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.7699999809265,
				pos = Vector(289.44555664063, 78.523681640625, 10.453125),
				ang = Angle(-74.47868347168, 34.795299530029, 154.96101379395),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_2 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 436,
				brightness = 2.77,
				pos = Vector(276.23291015625, -95.972396850586, 11.181640625),
				ang = Angle(-74.478675842285, -26.848712921143, 154.96101379395),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_3 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(110.03759765625, -257.27835083008, 20.2685546875),
				ang = Angle(-79.406143188477, -66.843933105469, 146.86637878418),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_4 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-58.076171875, -254.6669921875, 20.078125),
				ang = Angle(-79.406143188477, -98.392051696777, 146.86636352539),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_5 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-170.37475585938, -198.802734375, 12.78125),
				ang = Angle(-79.406143188477, -131.26023864746, 146.86636352539),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_6 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-206.408203125, 209.6337890625, 19.92578125),
				ang = Angle(-81.35474395752, 138.51747131348, 146.42248535156),
				shadows = true,

				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_7 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-104.0517578125, 256.6181640625, 13.9130859375),
				ang = Angle(-83.132545471191, 107.3970413208, 144.52496337891),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_8 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(78.666015625, 283.728515625, 15.9140625),
				ang = Angle(-79.406135559082, 64.003898620605, 146.86636352539),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_9 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(189.7607421875, 217.34164428711, 11.6376953125),
				ang = Angle(-79.406135559082, 30.60778427124, 146.86633300781),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_10 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-309.4169921875, -75.865936279297, 12.958984375),
				ang = Angle(-83.747146606445, 12.364686965942, -167.90577697754),
				shadows = true,

	
				warn = {
					color = Color(0,0,0)
				},
				off_warn = {
					color = Color(255, 245, 165)
				},
				off = {
					color = Color(255, 245, 165)
				},
			},
			lamp_11 = {
				color = Color(0, 143, 48),
				texture = "effects/flashlight/soft",
				fov = 143.63636779785,
				distance = 142.90908813477,
				brightness = 2.77,
				pos = Vector(-314.90112304688, 45.0693359375, 11.533203125),
				ang = Angle(-83.74715423584, -20.237749099731, -167.90577697754),
				shadows = true,
				},
				lamp_0_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 100,
					distance = 436,
					brightness = 2.7699999809265,
					pos = Vector(231.56860351563, -193.26171875, 18.859375),
					ang = Angle(-74.503662109375, -40.788387298584, 155.1356048584),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_1_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 100,
					distance = 436,
					brightness = 2.7699999809265,
					pos = Vector(289.44555664063, 78.523681640625, 10.453125),
					ang = Angle(-74.47868347168, 34.795299530029, 154.96101379395),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_2_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 100,
					distance = 436,
					brightness = 2.77,
					pos = Vector(276.23291015625, -95.972396850586, 11.181640625),
					ang = Angle(-74.478675842285, -26.848712921143, 154.96101379395),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_3_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(110.03759765625, -257.27835083008, 20.2685546875),
					ang = Angle(-79.406143188477, -66.843933105469, 146.86637878418),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_4_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(-58.076171875, -254.6669921875, 20.078125),
					ang = Angle(-79.406143188477, -98.392051696777, 146.86636352539),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_5_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(-170.37475585938, -198.802734375, 12.78125),
					ang = Angle(-79.406143188477, -131.26023864746, 146.86636352539),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_6_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(-206.408203125, 209.6337890625, 19.92578125),
					ang = Angle(-81.35474395752, 138.51747131348, 146.42248535156),
					shadows = true,
					nopower= true,
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_7_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(-104.0517578125, 256.6181640625, 13.9130859375),
					ang = Angle(-83.132545471191, 107.3970413208, 144.52496337891),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_8_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(78.666015625, 283.728515625, 15.9140625),
					ang = Angle(-79.406135559082, 64.003898620605, 146.86636352539),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_9_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(189.7607421875, 217.34164428711, 11.6376953125),
					ang = Angle(-79.406135559082, 30.60778427124, 146.86633300781),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_10_off = {
					color = Color(0, 143, 48),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(-309.4169921875, -75.865936279297, 12.958984375),
					ang = Angle(-83.747146606445, 12.364686965942, -167.90577697754),
					shadows = true,
					nopower= true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
				lamp_11_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 143.63636779785,
					distance = 142.90908813477,
					brightness = 2.77,
					pos = Vector(-314.90112304688, 45.0693359375, 11.533203125),
					ang = Angle(-83.74715423584, -20.237749099731, -167.90577697754),
					shadows = true,
					nopower= true,
					
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(179, 161, 3),
					},
					off = {
						color = Color(179, 161, 3),
					},
				},
	},
			TextureSets = {
				all_textures = {
					prefix = "models/tardisman/coral_10th/",
				},
				power_textures = {
					prefix = "models/tardisman/coral_10th/",
				},
			},
		},
	})

	TARDIS:AddInteriorTemplate("tardisman_colors_betahouse", {
		Interior = {
			LightOverride = {
				basebrightness = 0.00,
			},
			Light = {
				color = Color(16, 255, 56),
				warncolor = Color(16, 255, 16),
				brightness = 1,
				falloff = 5
			},
			Lamps = {
					lamp_2 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 1.59,
						pos = Vector(276.23291015625, -95.972396850586, 11.181640625),
						ang = Angle(-74.478675842285, -26.848712921143, 154.96101379395),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_3 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 1.59,
						pos = Vector(110.03759765625, -257.27835083008, 20.2685546875),
						ang = Angle(-79.406143188477, -66.843933105469, 146.86637878418),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_4 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 6.49,
						pos = Vector(-58.076171875, -254.6669921875, 20.078125),
						ang = Angle(-79.406143188477, -98.392051696777, 146.86636352539),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_5 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 6.49,
						pos = Vector(-170.37475585938, -198.802734375, 12.78125),
						ang = Angle(-79.406143188477, -131.26023864746, 146.86636352539),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_6 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 1.59,
						pos = Vector(-206.408203125, 209.6337890625, 19.92578125),
						ang = Angle(-81.35474395752, 138.51747131348, 146.42248535156),
						shadows = true,
		
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_7 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 1.59,
						pos = Vector(-104.0517578125, 256.6181640625, 13.9130859375),
						ang = Angle(-83.132545471191, 107.3970413208, 144.52496337891),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_8 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 6.49,
						pos = Vector(78.666015625, 283.728515625, 15.9140625),
						ang = Angle(-79.406135559082, 64.003898620605, 146.86636352539),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_9 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 6.49,
						pos = Vector(189.7607421875, 217.34164428711, 11.6376953125),
						ang = Angle(-79.406135559082, 30.60778427124, 146.86633300781),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_10 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 1.59,
						pos = Vector(-309.4169921875, -75.865936279297, 12.958984375),
						ang = Angle(-83.747146606445, 12.364686965942, -167.90577697754),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					lamp_11 = {
						color = Color(0, 139, 35),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 571.27,
						brightness = 1.59,
						pos = Vector(-314.90112304688, 45.0693359375, 11.533203125),
						ang = Angle(-83.74715423584, -20.237749099731, -167.90577697754),
						shadows = true,
		
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
				console_off = {
					color = Color(0, 0, 0),
					texture = "effects/flashlight/soft",
					fov = 123.63636016846,
					distance = 200,
					brightness = 0.4,
					pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
					ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
					shadows = true,
		
					nopower = true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(255, 245, 165)
					},
					off = {
						color = Color(255, 245, 165)
					},
				},
				door_off = {
					color = Color(0,0,0),
					texture = "effects/flashlight/soft",
					fov = 90,
					distance = 200,
					brightness = 0.3,
					pos = Vector(230, 0, 140),
					ang = Angle(70, 0, 0),
					shadows = false,
		
					nopower = true,
		
					warn = {
						color = Color(0,0,0)
					},
					off_warn = {
						color = Color(255, 245, 165)
					},
					off = {
						color = Color(255, 245, 165)
				},
			},
		},
				TextureSets = {
					all_textures = {
						prefix = "models/tardisman/coral_10th/",
					},
					power_textures = {
						prefix = "models/tardisman/coral_10th/",
					},
				},
			},
		})
	

		TARDIS:AddInteriorTemplate("tardisman_colors_2007_ex", {
			Interior = {
				LightOverride = {
					basebrightness = 0.17,
				},
				Lights = {
					{
						color = Color(255, 231, 94, 204),
						brightness = 0.8,
						warncolor = Color(182, 151, 83),
					},
					{
						color = Color(255, 187, 60, 206),
						brightness = 0.7,
						warncolor = Color(136, 113, 70),
					},
				},
				Lamps = {
					wall_1 = {
						color = Color(226, 194, 164),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 524,
						brightness = 1.1,
						pos = Vector(35.08837890625, -66.75048828125, 215.359375),
						ang = Angle(68.821922302246, -57.793685913086, -122.65737915039),
		
						warn = {
							color = Color(155,50,50)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
					wall_2 = {
						color = Color(226, 194, 164),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 524,
						brightness = 1.1,
						pos = Vector(-84.211181640625, -2.478515625, 218.2197265625),
						ang = Angle(67.282157897949, -174.31925964355, -86.926818847656),
		
						warn = {
							color = Color(155,50,50)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
					wall_3 = {
						color = Color(226, 194, 164),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 524,
						brightness = 1.1,
						pos = Vector(21.555786132813, 74.853515625, 217.6494140625),
						ang = Angle(72.478538513184, 72.291961669922, -137.94195556641),
		
						warn = {
							color = Color(155,50,50)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
					wall_5 = {
						color = Color(226, 194, 164),
						texture = "effects/flashlight/soft",
						fov = 76.363639831543,
						distance = 200,
						brightness = 7.3181819915771,
						pos = Vector(127.5927734375, -164.9951171875, 187.595703125),
						ang = Angle(-5.2171516418457, -55.968048095703, 157.88014221191),

						warn = {
							color = Color(155,50,50)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
					wall_6 = {
						color = Color(226, 194, 164),
						texture = "effects/flashlight/soft",
						fov = 76.363639831543,
						distance = 200,
						brightness = 7.3181819915771,
						pos = Vector(208.609375, -123.1376953125, 163.060546875),
						ang = Angle(-24.19903755188, -62.307903289795, 158.96154785156),
					

						warn = {
							color = Color(155,50,50)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
					wall_7 = {
						color = Color(226, 194, 164),
						texture = "effects/flashlight/soft",
						fov = 76.363639831543,
						distance = 200,
						brightness = 7.3181819915771,
						pos = Vector(-40.0947265625, -206.2587890625, 164.1162109375),
						ang = Angle(-22.561664581299, -97.030097961426, 163.06370544434),
					
						warn = {
							color = Color(155,50,50)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
					wall_8 = {
					color = Color(226, 194, 164),
					texture = "effects/flashlight/soft",
					fov = 76.363639831543,
					distance = 200,
					brightness = 7.3181819915771,
					pos = Vector(-163.876953125, -138.4189453125, 173.6064453125),
					ang = Angle(-20.746122360229, -154.75207519531, 158.82852172852),


						warn = {
							color = Color(155,50,50)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
					console_off = {
						color = Color(0, 0, 0),
						texture = "effects/flashlight/soft",
						fov = 123.63636016846,
						distance = 200,
						brightness = 0.4,
						pos = Vector(-0.2646484375, 1.183837890625, 173.7490234375),
						ang = Angle(88.889175415039, 66.60807800293, -17.834625244141),
						shadows = true,
			
						nopower = true,
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					door_off = {
						color = Color(0,0,0),
						texture = "effects/flashlight/soft",
						fov = 90,
						distance = 200,
						brightness = 0.3,
						pos = Vector(230, 0, 140),
						ang = Angle(70, 0, 0),
						shadows = false,
			
						nopower = true,
			
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(255, 245, 165)
						},
						off = {
							color = Color(255, 245, 165)
						},
					},
					wall_4 = {
						color = Color(226, 194, 164),
						texture = "effects/flashlight/soft",
						fov = 170,
						distance = 300,
						brightness = 0.25,
						pos = Vector(233.13061523438, -0.66015625, 219.779296875),
						ang = Angle(52.830783843994, -177.60000610352, -163.38583374023),
		
						warn = {
							color = Color(0,0,0)
						},
						off_warn = {
							color = Color(180,0,0)
						},
					},
				},
				TextureSets = {
					all_textures = {
						prefix = "models/tardisman/coral_10th/",
					},
					power_textures = {
						prefix = "models/tardisman/coral_10th/",
					},
				},
			},
		})
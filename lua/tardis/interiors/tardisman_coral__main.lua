local function select_s(setting, id, ply, ent)
	return TARDIS:GetCustomSetting(id, setting, ply)
end

local T = {
	ID = "tardisman_coral",
	Name = "2005 by TARDISman",
	Base = "tardis2005",
}
T.Versions = {
	randomize = false,
    allow_custom = false,

    main = {
        id = "tardisman_coral",
    },
    other = {
        {
            name = "2005 TARDIS (A/B Box)",
            id = "tardisman_coral_2005_ab",
        },
        {
            name = "2005 TARDIS (C Box)",
            id = "tardisman_coral_2005_c",
        },
		{
			name = "2006 TARDIS (A/B Box)",
			id = "tardisman_coral_2006_ab",
        },
		{
			name = "2007 TARDIS (A/B Box)",
			id = "tardisman_coral_2007_ab",
		},
		{
			name = "2008 TARDIS (C Box)",
			id = "tardisman_coral_2008_c",
		},
		{
			name = "2009 TARDIS (C Box)",
			id = "tardisman_coral_2009_c",
		},
		{
			name = "DOTD TARDIS (A/B Box)",
			id = "tardisman_coral_dotd_ab",
		},
		{
            name = "Custom (selected in settings)",
            id = "tardisman_coral",
        },
    },
}

T.CustomSettings = {
	interior_color = {
		text = "Interior colors",
		value_type = "list",
		value = "07ex",
		options = {
			["05"] = "2005 Lighting",
			["06"] = "2006 Lighting",
			["07"] = "2007 Lighting",
			["08"] = "2008 Lighting",
			["09"] = "2009 Lighting (End Of Time)",
			["07ex"] = "2007 Lighting (Alt)",
			["ho"] = "House Lighting (Experimental)",
			["hoex"] = "Custom Green Lighting (Old House)",
			["dotd"] = "DOTD Lighting",
		}
	},
	exterior = {
		text = "Exterior",
		value_type = "list",
		value = "AB_BOX",
		options = {
			["C_BOX"] = "2008-2009 C Box",
			["AB_BOX"] = "2005-2007 A/B Box",
			["OLD"] = "Old A/B Box",
		}
	},
	sounds = {
		text = "Sounds",
		value_type = "list",
		value = "2005",
		options = {
			["2005"] = "2005",
			["2006"] = "2006-2008",
			["2009"] = "2009 (End Of Time)",
			["house"] = "House Controlled (The Doctor's Wife)",
		}
	},
}
T.Templates = {
	----------------------------------------------------------------------------
	-- Texture sets
	----------------------------------------------------------------------------
	tardisman_coral_texture_sets_interior = { override = true, },
	-- we always need this, so no condition

	tardisman_coral_texture_sets_exterior_ab = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("exterior", id, ply, ent)
			return (selected == "AB_BOX")
		end,
	},
	----------------------------------------------------------------------------
	-- Color
	----------------------------------------------------------------------------
	tardisman_colors_2005 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "05")
		end,
	},
	tardisman_colors_2006 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "06")
		end,
	},
	tardisman_colors_2007 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "07")
		end,
	},
	tardisman_colors_2008 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "08")
		end,
	},
	tardisman_colors_2009 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "09")
		end,
	},
	tardisman_colors_2007_ex = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "07ex")
		end,
	},
	tardisman_colors_house = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "ho")
		end,
	},
	tardisman_colors_betahouse = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "hoex")
		end,
	},
	tardisman_colors_DOTD = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("interior_color", id, ply, ent)
			return (selected == "dotd")
		end,
	},

	----------------------------------------------------------------------------
	-- Exterior model type
	----------------------------------------------------------------------------
	tardisman_box_common = { override = true, },

	tardisman_ab_box = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("exterior", id, ply, ent)
			return (selected == "AB_BOX")
		end,
	},
	tardisman_c_box = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("exterior", id, ply, ent)
			return (selected == "C_BOX")
		end,
	},

	----------------------------------------------------------------------------
	-- Demat and Mat sounds
	----------------------------------------------------------------------------

	tardisman_coral_sounds_2005 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("sounds", id, ply, ent)
			return (selected == "2005")
		end,
	},

	tardisman_coral_sounds_2006_2008 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("sounds", id, ply, ent)
			return (selected == "2006")
		end,
	},

	tardisman_coral_sounds_2009 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("sounds", id, ply, ent)
			return (selected == "2009")
		end,
	},

	tardisman_coral_sounds_house = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_s("sounds", id, ply, ent)
			return (selected == "house")
		end,
	},
	
}

TARDIS:AddInterior(T)

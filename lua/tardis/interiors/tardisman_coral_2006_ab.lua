-- 2005 TARDIS

local T = {
	Base = "tardis2005",
	IsVersionOf = "tardisman_coral",
	Name = "2006 TARDIS (A/B Box)",
	ID = "tardisman_coral_2006_ab",
}
T.Interior = {
	Sequences = "tardis2006retexture_sequences",
	Tips = {
		style = "retexturecoral",
		view_range_max = 70,
		view_range_min = 50,
	},
	PartTips = {
		tardis2005_rheostat = 		{pos = Vector(38.018, 0.284, 77.1), 	text = "Rheostat"},
		tardis2005_button = 		{pos = Vector(26.81, 18.034, 73.298), 	text = "Intercom"},
	},
}
T.Templates = {
	tardisman_colors_2006 = {override = true},
	tardisman_coral_texture_sets_interior = {override = true,},
	tardisman_coral_texture_sets_exterior_ab = {override = true,},
	tardisman_box_common = {override = true,},
	tardisman_ab_box = {override = true,},
	tardisman_coral_sounds_2006_2008 = {override = true},
}

TARDIS:AddInterior(T)

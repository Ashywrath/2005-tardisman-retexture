local id = "tardisman_coral_2005_c"
local T = TARDIS:CreateInteriorMetadata(id)

local E = T.Exterior
E.ID = "tardisman_coral_c_box"
E.Base = "base"
E.Name = "2005-2009 C Box (Retexture)"
E.Category = "Exteriors.Categories.PoliceBoxes"

TARDIS:AddExterior(E)

local id = "tardisman_coral_2005_ab"
local T = TARDIS:CreateInteriorMetadata(id)
local E = T.Exterior

E.ID = "tardisman_coral_ab_box"
E.Base = "base"
E.Name = "2005-2009 A/B Box (Retexture)"
E.Category = "Exteriors.Categories.PoliceBoxes"

TARDIS:AddExterior(E)

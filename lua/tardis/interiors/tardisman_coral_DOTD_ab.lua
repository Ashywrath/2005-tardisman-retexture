

local T = {
	Base = "tardis2005",
	IsVersionOf = "tardisman_coral",
	Name = "DOTD TARDIS (AB Box)",
	ID = "tardisman_coral_dotd_ab",
}
T.Interior = {
	Sequences = "tardis2009retexture_sequences",
	Tips = {
		style = "retexturecoral",
		view_range_max = 70,
		view_range_min = 50,
	},
	PartTips = {
		tardis2005_rheostat = 		{pos = Vector(38.018, 0.284, 77.1), 	text = "Rheostat"},
		tardis2005_button = 		{pos = Vector(26.81, 18.034, 73.298), 	text = "Intercom"},
	},
}
T.Templates = {
	tardisman_colors_DOTD = {override = true},
	tardisman_coral_texture_sets_interior = {},
	tardisman_coral_texture_sets_exterior_ab = {override = true,},
	tardisman_box_common = {override = true,},
	tardisman_ab_box = {override = true},
	tardisman_coral_sounds_2009 = {override = true},
}

TARDIS:AddInterior(T)
